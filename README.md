# README #

## Determining Prevalence and Seroprevalence of COVID-19 in Metro Louisville, Kentucky: A Bayesian Approach

### Authors: Grzegorze Rempała, Chance Alvarado, Krzysztof Banecki

In the following analysis, we aim to use a Bayesian approach coupled with a modified version of the classical SIR epidemiological model to provide estimates for seroprevalence and prevalence of COVID-19 in the adult population of the Louisville metro area in Kentucky.


