if (init_vals=="_w1-4"){
  default_init <- FALSE
  stan_init_vector <- c(data.S$pseudo_med[length(data.S$pseudo_med)],
                        data.I$pseudo_med[length(data.I$pseudo_med)],
                        data.T$pseudo_med[length(data.R$pseudo_med)],
                        data.R$pseudo_med[length(data.T$pseudo_med)])
  init_vectors <- data.frame(cbind(trajectory.matrix.S[dim(trajectory.matrix.S)[1],],
                                   trajectory.matrix.I[dim(trajectory.matrix.I)[1],],
                                   trajectory.matrix.R[dim(trajectory.matrix.R)[1],],
                                   trajectory.matrix.T[dim(trajectory.matrix.T)[1],]))
  colnames(init_vectors) <- c("S", "I", "R", "T")
  
} else {
  default_init <- TRUE
  stan_init_vector <- c(NA, NA, NA, NA)
}


initial.vector <- c(0.35, 0.25, 0.35, 0.05)
total <- initial.vector[1]+initial.vector[4]
ex <- initial.vector[1]/(total)
var <- 0.001
new_proportion <- rbeta(1, (1-ex)*ex^2/var-ex, (1-ex)^2*ex/var-(1-ex))
initial.vector.current <- c(total*new_proportion, initial.vector[2], initial.vector[3], total*(1-new_proportion))
initial.vector.current

setwd('~/Documents/Doktorat/covid-modelling/covid_prevalence')

#setwd('~/Documents/Doktorat/covid-modelling/Archive/plots-GR4')
