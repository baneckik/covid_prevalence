
functions {
  
  real[] SIRT(real t , real[] y , real[] parms , real[] rdata , int[] idata){
    
    vector[2] betagamma = [parms[1], parms[2]]';    // Infection rate
    //real gamma = parms[2];   // Recovery rate
    real delta = parms[3];   // Rate recovered produce antibodies
    
    real rho = parms[4];     // Initially infected
    real eps = parms[5];     // Initially recovered
    real psi = parms[6];     // Initial amount with antibodies
    
    real spec = parms[7];     // test specificity
    
    real dydt[4];            // System of ODES
    dydt[1] = -exp(betagamma[1]) *y[1] * y[2];                // dS/dt
    dydt[2] = exp(betagamma[1])*y[1] * y[2] - exp(betagamma[2])*y[2];      // dI/dt
    dydt[3] = exp(betagamma[2])*y[2] - delta*y[3];        // dR/dt
    dydt[4] = delta*y[3];                     // dT/dt
    
    return dydt;           
  }
  
  real PR(real pob , real spec){
  
  real sens = 1;
  real val;
  
  val = sens * pob + (1 - spec) * (1 - pob);
  return val; 
  }
  
}

data {
  
  int<lower=0> k;           //number of time points  
  real<lower=0> t0;         // initial time 
  real<lower=0> ti[k];      // test time (in days)
  real<lower=0> xi[k];      // positive tests  
  real<lower=0> ni[k];      // total tests 
  real<lower=0, upper=1> init_vec[4];
}

transformed data{
  
  real x_r[0];
  int x_i[0];
  
}

parameters {
  
  row_vector<lower=0.0>[2] betagamma;      
  real<lower=0.0> delta;    
  
  real<lower=0.0> rho;      
  real<lower=0.0> eps;
  real<lower=0.0> psi;     
  real<lower=0.0, upper=1.0> spec;  
  
}

transformed parameters{
  vector<lower=0>[2] mu;
  cov_matrix[2] sigma;
  
  mu[1] = 0.28;
  mu[2] = 0.28;
  sigma[1,1] = 0.003;
  sigma[1,2] = 0.002;
  sigma[2,1] = 0.002;
  sigma[2,2] = 0.003;
}

model {
  
  real temp[k,4];     // Solution matrix
  real parms[7];      // Parameters vector
  real init[4];       // Initial condition vector

  parms[1] = betagamma[1];
  parms[2] = betagamma[2];
  parms[3] = delta;
  parms[4] = rho;
  parms[5] = eps; 
  parms[6] = psi;

  parms[7] = spec;

  if (sum(init_vec)==0){
    init[1] = 1 - rho - eps - psi;      // Initial number susceptible
    init[2] = rho;                      // Initial number infected
    init[3] = eps;                      // Initial number recovered
    init[4] = psi;                      // Initial number seroprevalent
  } else {
    init[1] = init_vec[1];      // Initial number susceptible
    init[2] = init_vec[2];                       // Initial number infected
    init[3] = init_vec[3];                       // Initial number recovered
    init[4] = init_vec[4];                       // Initial number seroprevalent
  }
  
  temp = integrate_ode_rk45(SIRT,init,t0,ti,parms,x_r,x_i,  // Solution matrix
                            1.0E-6,1.0E-6,1.0E6); 
  
  for(i in 1:k){
    
    target += xi[i] * log(PR(temp[i,4],spec));           // Likelihood of positives
    target += (ni[i]-xi[i]) * log(1-PR(temp[i,4],spec)); // Likelihood of negatives
    
  }
  
  // target += gamma_lpdf(beta| 8.74, 30.88);
  // target += gamma_lpdf(gamma| 7.85, 30.70);
  // target += gamma_lpdf(delta| 25.41, 239.25);
  // target += gamma_lpdf(rho| 2.91, 11206.96);
  // target += gamma_lpdf(eps| 1.79, 854.29);
  // target += gamma_lpdf(psi| 112, 5112.49);
  // target += beta_lpdf(spec| 100, 100/0.76-100);
  
  target += multi_normal_lpdf(betagamma | mu, sigma);
  target += gamma_lpdf(delta| 25.41, 239.25);
  target += gamma_lpdf(rho| 3.1, 11206.96);
  target += gamma_lpdf(eps| 1.79, 854.29);
  target += gamma_lpdf(psi| 112, 5112.49);
  target += beta_lpdf(spec| 109, 3.83);
  
}
